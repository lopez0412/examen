package com.example.lopez.examenandroid1;

import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebService extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{
    GoogleMap mMap;
    SupportMapFragment mapFragment;
    List<Coordenadas> coordenadas=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_service);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
        enviar();
    }

    public void enviar() {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            String URL = "http://74.205.41.248:8081/pruebawebservice/api/mob_sp_GetArchivo";
            final JSONObject jsonBody = new JSONObject();
            jsonBody.put("serviceId", 55);
            jsonBody.put("userId", 12984);
            final String mRequestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                private String urldownload;
                private boolean error;
                private int id;

                @Override
                public void onResponse(String response) {
                    try {


                        JSONArray array = new JSONArray(response);
                        JSONObject object = array.getJSONObject(0);
                        id = object.getInt("code");
                        error = object.getBoolean("hasError");
                        urldownload = object.getString("valueResponse");


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    File archivo = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "archivo");
                    archivo.mkdirs();
                    //downloadZipFile(urldownload, Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+ File.separator+"archivo"+ File.separator  + "archivo.zip");
                    downloadAndUnzipContent(urldownload);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("LOG_VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                        return null;
                    }
                }


                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {

                        responseString = String.valueOf(response.statusCode);


                    }
                    return super.parseNetworkResponse(response);
                }
            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void downloadAndUnzipContent(String url) {
        coordenadas.clear();

        DownloadFileAsync download = new DownloadFileAsync(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + File.separator + "archivo" + File.separator + "archivo.zip", this, new DownloadFileAsync.PostDownload() {
            @Override
            public void downloadDone(File file) {
                //Log.i(TAG, "file download completed");

                // check unzip file now
                Descomprimir unzip = new Descomprimir(WebService.this, file);
                unzip.unzip();
                String mFileContent = "";
                try {
                    mFileContent = loadFileContent();
                    JSONObject mjson=new JSONObject(mFileContent);
                    JSONArray array=mjson.getJSONArray("data");
                    for (int i=0;i<array.length();i++) {
                        JSONObject mJsonObj = array.getJSONObject(i);
                        JSONArray array1=mJsonObj.getJSONArray("UBICACIONES");
                        for (int a=0;a<array1.length();a++){
                            JSONObject mJsonObj1 = array1.getJSONObject(a);
                            String latitud=mJsonObj1.getString("FNLATITUD");
                            String LONGITUD=mJsonObj1.getString("FNLONGITUD");
                            Coordenadas c=new Coordenadas();
                            c.setLatitud(Double.valueOf(latitud));
                            c.setLongitud(Double.valueOf(LONGITUD));


                            coordenadas.add(c);
                        }

                    }

                    cargarmarcadores();

                } catch (Throwable t) {
                    Log.d("TAG", "Could not parse malformed JSON: \n" + mFileContent);
                }


                //Log.i(TAG, "file unzip completed");
            }


        });
        download.execute(url);
    }

    private void cargarmarcadores() {
        for (int i = 0; i < coordenadas.size(); i++) {
            LatLng ltln=new LatLng(coordenadas.get(i).getLatitud(),coordenadas.get(i).getLongitud());
            mMap.addMarker(new MarkerOptions().position(ltln));

        }
    }

    private String loadFileContent() throws IOException {
        String mFilePath = "/storage/emulated/0/Documents/archivo/cargaInicial_2019-03-15_12984.txt";
        File yourFile = new File(mFilePath);
        FileInputStream fos = new FileInputStream(yourFile);
        String fileContent = "";
        try {
            FileChannel fc = fos.getChannel();
            MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            /* Instead of using default, pass in a decoder. */
            fileContent = Charset.defaultCharset().decode(bb).toString();
        }
        finally {
            fos.close();
        }

        return fileContent;
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        if (ActivityCompat.checkSelfPermission(WebService.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(WebService.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            mMap.setMyLocationEnabled(true);
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setScrollGesturesEnabled(true);
            mMap.getUiSettings().setAllGesturesEnabled(true);
            LatLng latLng = new LatLng(13.678654, -89.254249);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,2));




            //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,16));
            // MapRipple mapRipple = new MapRipple(mMap, latLng, getContext());
            // mapRipple.withDistance(300);
            // mapRipple.withRippleDuration(5000);
            // mapRipple.startRippleMapAnimation();      //in onMapReadyCallBack


        } else {
            ActivityCompat.requestPermissions(WebService.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
        }

    }
}
