package com.example.lopez.examenandroid1;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class Hilo_notificacion extends AppCompatActivity {
    static Context thisActivity = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hilo_notificacion);

        thisActivity =  getApplicationContext();

        RelativeLayout rl= findViewById(R.id.relative1);

        Button btnIniciar = new Button(this);
        btnIniciar.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        btnIniciar.setText("Iniciar");

        rl.addView(btnIniciar);

        btnIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Handler handler=new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mensaje();
                    }
                },10000);

            }
        });



    }
    public  void mensaje() {

        Toast.makeText(this,"El tiempo ha terminado",Toast.LENGTH_LONG).show();
    }


}
