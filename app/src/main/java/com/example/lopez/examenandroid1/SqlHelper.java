package com.example.lopez.examenandroid1;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SqlHelper extends SQLiteOpenHelper {
    private static final String BASEDATOS = "DBExamen";
    //versi�n de la base de datos
    private static final int VERSION = 1;
    public static final String tabla1="datos";

    public SqlHelper( Context context) {
        super(context,BASEDATOS,null,VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("Create table "+tabla1+"(id integer, nombre text,fecha_nacimiento text,puesto text,primary key (id))");

        db.execSQL("Insert into "+tabla1+"  (nombre,fecha_nacimiento,puesto) VALUES ('Miguel Cervantes','08-Dic-1990','Desarrollador')");
        db.execSQL("Insert into "+tabla1+"  (nombre,fecha_nacimiento,puesto) VALUES ('Juan Morales','03-Jul-1990','Desarrollador')");
        db.execSQL("Insert into "+tabla1+"  (nombre,fecha_nacimiento,puesto) VALUES ('Roberto Mendez','14-Oct-1990','Desarrollador')");
        db.execSQL("Insert into "+tabla1+"  (nombre,fecha_nacimiento,puesto) VALUES ('Miguel Cuevas','08-Dic-1990','Desarrollador')");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
