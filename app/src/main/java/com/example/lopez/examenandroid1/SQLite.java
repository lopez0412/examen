package com.example.lopez.examenandroid1;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class SQLite {
    private SqlHelper sqlhelper;

    private SQLiteDatabase db;

    /** Constructor de clase */
    public SQLite(Context context)
    {
        sqlhelper = new SqlHelper( context );
    }

    public void abrir(){
        Log.i("SQLite", "Se abre conexion a la base de datos " + sqlhelper.getDatabaseName() );
        db = sqlhelper.getWritableDatabase();
    }

    /** Cierra conexion a la base de datos */
    @SuppressLint("NewApi")
    public void cerrar()
    {
        Log.i("SQLite", "Se cierra conexion a la base de datos " + sqlhelper.getDatabaseName() );
        sqlhelper.close();
    }
    public boolean agregarRegistro( int id, String nombre ,String fecha,String puesto)
    {
        if( nombre.length()> 0 )
        {
            ContentValues Values = new ContentValues();

            Values.put("nombre" , nombre );
            Values.put("fecha",fecha);
            Values.put("puesto",puesto);
            Log.i("SQLite", "Nuevo registro "+sqlhelper.tabla1 );
            return ( db.insert( sqlhelper.tabla1 , null, Values ) != -1 )?true:false;

        }
        else
            return false;
    }

    public Cursor consultarCarrito()
    {
        return db.query( sqlhelper.tabla1 ,
                new String[]{
                        "id",
                        "nombre",
                        "fecha_naciemiento",
                        "puesto"



                },
                null, null, "id", null, null);
    }
}
